<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialColumnsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('social_vk_id', 100);
			$table->string('social_twitter_id', 100);
			$table->string('social_odnoklassniki_id', 100);
			$table->string('social_google_id', 100);
			$table->string('social_facebook_id', 100);
			$table->string('social_github_id', 100);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->dropColumn('social_vk_id');
            $table->dropColumn('social_twitter_id');
            $table->dropColumn('social_odnoklassniki_id');
            $table->dropColumn('social_google_id');
            $table->dropColumn('social_facebook_id');
            $table->dropColumn('social_github_id');
		});
	}

}
