<?php
/**
 * Created by PhpStorm.
 * User: damned
 * Date: 2/20/15
 * Time: 4:17 AM
 */

namespace Spark\SocialAuth\Providers;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

/**
 * Class OdnoklassnikiProvider
 * @package Spark\SocialAuth\Providers
 *
 * Full api docs here: http://apiok.ru/wiki/display/api/Odnoklassniki+REST+API+ru
 */
class OdnoklassnikiProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * @var array Profile fields to return. Full list of supported parameters here: http://apiok.ru/wiki/display/api/users.getCurrentUser+ru
     */
    protected $userProfileFields = [
        'UID',
        'LOCALE',
        'NAME',
        'PIC1024x768'
    ];

    /**
     * Get the authentication URL for the provider.
     *
     * @param  string $state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('http://www.odnoklassniki.ru/oauth/authorize', $state);
    }

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    protected function getTokenUrl()
    {
        return 'https://api.odnoklassniki.ru/oauth/token.do';
    }

    /**
     * Get the access token for the given code.
     *
     * @param  string  $code
     * @return string
     */
    public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'body' => array_add($this->getTokenFields($code), 'grant_type', 'authorization_code'),
        ]);

        return $this->parseAccessToken($response->getBody());
    }


    /**
     * Get the raw user for the given access token.
     *
     * @param  string $token
     * @return array
     */
    protected function getUserByToken($token)
    {
        $fieldsString = implode(',', $this->userProfileFields);

        $params = [
            'sig'               =>  strtolower(md5('application_key='.config('services.odnoklassniki.public_key').'fields='.$fieldsString.md5($token.config('services.odnoklassniki.client_secret')))),
            'application_key'   =>  config('services.odnoklassniki.public_key'),
            'access_token'      =>  $token,
            'fields'            =>  $fieldsString
        ];

        $response = $this->getHttpClient()->get(
            'http://api.ok.ru/api/users/getCurrentUser',
            [
                'query'  =>  [
                    $params
                ],
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param  array $user
     * @return \Laravel\Socialite\User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'            => $user['uid'],
            'nickname'      => null,
            'name'          => $user['name'],
            'email'         => null,
            'avatar'        => $user['pic1024x768'],
        ]);
    }
}