<?php
/**
 * Created by PhpStorm.
 * User: damned
 * Date: 2/20/15
 * Time: 1:32 AM
 */

namespace Spark\SocialAuth\Providers;


use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class VkProvider extends AbstractProvider implements ProviderInterface
{
    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = ['friends,email'];

    /**
     * @var array Profile fields to return. Full list of supported parameters here: https://vk.com/dev/fields
     */
    protected $userProfileFields = [
        'photo_max_orig',
        'city',
        'verified',
        'screen_name',
        'uid',
        'first_name',
        'last_name',
        'sex',
        'bdate'
    ];

    /**
     * @var string User email. Will return with access_token
     */
    protected $email;

    /**
     * Get the authentication URL for the provider.
     *
     * @param  string $state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://oauth.vk.com/authorize', $state);
    }

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    protected function getTokenUrl()
    {
        return 'https://oauth.vk.com/access_token';
    }

    /**
     * Get the access token from the token response body.
     *
     * @param  string  $body
     * @return string
     */
    protected function parseAccessToken($body)
    {
        $decodedBody = json_decode($body, true);
        $this->email = $decodedBody['email'];

        return $decodedBody['access_token'];
    }

    /**
     * Get the access token for the given code.
     *
     * @param  string  $code
     * @return string
     */
    public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->get($this->getTokenUrl(), [
            'query' => $this->getTokenFields($code),
        ]);

        return $this->parseAccessToken($response->getBody());
    }


    /**
     * Get the raw user for the given access token.
     *
     * @param  string $token
     * @return array
     */
    protected function getUserByToken($token)
    {
        $params = [
            'access_token'  =>  $token,
            'fields'        =>  implode(',', $this->userProfileFields)
        ];

        $response = $this->getHttpClient()->get('https://api.vk.com/method/users.get?'.http_build_query($params), [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        return head(json_decode($response->getBody(), true)['response']);
    }

    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param  array $user
     * @return \Laravel\Socialite\Contracts\User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'            => $user['uid'],
            'nickname'      => null,
            'name'          => $user['first_name'].' '.$user['last_name'],
            'email'         => $this->email,
            'avatar'        => $user['photo_max_orig'],
        ]);
    }
}