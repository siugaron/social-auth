<?php namespace Spark\SocialAuth;

use Illuminate\Support\ServiceProvider;

class SocialAuthServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

    public function boot()
    {
        /**
        * Publish migrates
        */
        $this->publishes([
            realpath(__DIR__.'/../../database/migrations') => $this->app->databasePath().'/migrations',
        ]);

        \App::register('Laravel\Socialite\SocialiteServiceProvider');

        \Socialize::extend('vk', function($app){
            return \Socialize::buildProvider('Spark\SocialAuth\Providers\VkProvider', config('services.vk'));
        });
        \Socialize::extend('odnoklassniki', function($app){
            return \Socialize::buildProvider('Spark\SocialAuth\Providers\OdnoklassnikiProvider', config('services.odnoklassniki'));
        });
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
