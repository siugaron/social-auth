<?php
/**
 * Created by PhpStorm.
 * User: damned
 * Date: 2/20/15
 * Time: 6:38 AM
 */

namespace Spark\SocialAuth\Traits;

use Carbon\Carbon;
use Laravel\Socialite\Contracts\User as SocialUser;
use Uploader;

/**
 * Class EloquentRepositorySocialiteTrait
 * @package Spark\SocialAuth\Traits
 *
 * Трейт для репозитория пользователей
 */
trait EloquentRepositorySocialiteTrait
{
    /**
     * Регистрирует пользователя из соц. сети
     *
     * @param SocialUser $socUser
     * @param string $provider название провайдера соц.сети
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function registerSocialUser(SocialUser $socUser, $provider)
    {
        /**
         * @ToDo: Перенести логику привязки соц.сети к пользователю в команду.
         */
        if (mb_strlen($socUser->getEmail(), 'UTF-8') > 0) {
            $user = $this->getByEmail($socUser->getEmail());

            if ($user !== null) {
                $user->setAttribute('social_'.$provider.'_id', $socUser->getId());
                $user->save();

                return $user;
            }
        }

        $data = [];
        $arNames = explode(' ',$socUser->getName(),2);
        $data['name']                       =   $arNames[0];
        $data['second_name']                =   (isset($arNames[1]))?$arNames[1]:'';
        $data['email']                      =   $socUser->getEmail();
        if(isset($socUser->user['bdate'])) {
            $data['birthday']               =   date('d.m.Y',(int)Carbon::createFromFormat('j.n.Y',$socUser->user['bdate'])->timestamp);
            $methodUrl = 'http://api.vk.com/method/database.getCitiesById?city_ids='.$socUser->user['city'];
            $json = file_get_contents($methodUrl, false);
            $arCities = json_decode($json, true);
            $data['city'] = $arCities['response'][0]['name'];
        }
        if(isset($socUser->user['sex'])) {
            $data['gender'] = ($socUser->user['sex']==1)?'male':'female';
        }elseif (isset($socUser->user['gender'])) {
            $data['gender'] = $socUser->user['gender'];
        }

        $newUser = $this->model->newInstance();
        $newUser->setAttribute('social_'.$provider.'_id', $socUser->getId());

        if (mb_strlen($socUser->getAvatar(), 'UTF-8') > 0) {
            $file = Uploader::receiveFromUrl($socUser->getAvatar());

            if ($file !== null) {
                $newUser->setAttribute('avatar', $file->getKey());
            }
        }

        return $newUser->register($data);
    }

    /**
     * Возвращает пользователя из таблицы по пользователю соц.сети
     *
     * @param SocialUser $user
     * @param $provider
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function getBySocialUser(SocialUser $user, $provider)
    {
        if($user->getEmail()) {
            return $this->model->where('social_' . $provider . '_id', '=', $user->getId())->orWhere('email', '=', $user->getEmail())->first();
        } else {
            return $this->model->where('social_' . $provider . '_id', '=', $user->getId())->first();
        }
    }
} 