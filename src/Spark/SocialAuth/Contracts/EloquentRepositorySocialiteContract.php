<?php
/**
 * Created by PhpStorm.
 * User: damned
 * Date: 2/20/15
 * Time: 6:42 AM
 */

namespace Spark\SocialAuth\Contracts;

use Laravel\Socialite\Contracts\User as SocialUser;

interface EloquentRepositorySocialiteContract {
    /**
     * Регистрирует пользователя из соц. сети
     *
     * @param SocialUser $user
     * @param string $provider название провайдера соц.сети
     *
     * @return \Cyl\Users\Models\User
     */
    public function registerSocialUser(SocialUser $user, $provider);

    /**
     * Возвращает пользователя из таблицы по пользователю соц.сети
     *
     * @param SocialUser $user
     * @param $provider
     *
     * @return \Cyl\Users\Models\User|null
     */
    public function getBySocialUser(SocialUser $user, $provider);
} 